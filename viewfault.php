<?php
//AUTHOR - STEPHANIE DELORENZIS 

//each page requires a header
require_once("includes/header.php");

?>
<!-- each page needs to be in a container -->
<main role="main" class="container-fluid">
		
	<h1>Registered Faults</h1>
	<div class="row">
		<div class="col-xs-12 col-md-3 offset-md-2">
			<a role="button" class="btn btn-success" href="addfault.php"><i class="fas fa-plus"></i> Add Fault</a>
		</div>		
	</div>
	<br/>
	<div class="row">
		<div class="col-xs-12 col-md-8 offset-md-2">
			<table class="table" id="faulttable">
				<thead>
					<tr>
						<th scope = "col">Date Registered</th>
						<th scope = "col">Fault</th>
						<th scope = "col"/> 
					</tr>
				</thead>
				<tbody>
				
				</tbody>
			</table>
		</div>
	</div>
	
	<!-- TOAST to display when data is saved -->
	<div class="toast" role="alert" aria-live="polite" aria-atomic="true" data-delay="1000">
	  <div class="toast-header">
		<strong class="mr-auto">CheckYourCar</strong>
		<small class="text-muted">just now</small>
		<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
	  <div class="toast-body">
		Fault Deleted
	  </div>
	</div>
</main>

<?php

//each page requires a footer
require_once("includes/footer.php");
?>

<script type="text/javascript" charset="utf">
(function($) {
	loadFaults();	
}(jQuery));

//Load all the saved faults into the table
function loadFaults() {
	$.getJSON("includes/query.php?function=getFaults", function(response) {
		$("#faulttable tbody").empty();
		$.each(response.result, function() {
			var date = new Date(this.timeadded);
			var dateStr = date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear();
			
			$("#faulttable tbody").append($("<tr/>").attr("class","fault_row").attr("id", "fault_"+this.faultid)
				.append($("<td/>").text(dateStr))
				.append($("<td/>").text(this.faultdesc))
				.append($("<td/>").append($("<a/>").attr("role", "button").attr("class", "btn btn-secondary btn_edit").attr("href", "addfault.php?"+this.faultid).html("<i class=\"fas fa-edit\"></i>"))
									.append($("<button/>").attr("type", "button").attr("class", "btn btn-danger btn_remove").attr("onclick", "removeFault("+this.faultid+")").html("<i class=\"fas fa-trash\"></i>"))));
		});
	});	
}

//Delete a fault
function removeFault(faultid) {
	$.getJSON("includes/query.php?function=removeFault&faultid="+faultid, function(response) {
		if(response.code == 200) {
			loadFaults();
		} else {
			//something went wrong
		}
	});
	
	
	//display a toast
	$(".toast").toast({delay:2000}).toast("show");
}

</script>