<?php
//AUTHOR - STEPHANIE DELORENZIS
//each page requires a header
require_once("includes/header.php");
$faultid = null;
//use the query string to get the faultid
$queryStr = $_SERVER["QUERY_STRING"];
if(is_numeric($queryStr)) {
	$faultid = intval($queryStr);
}

?>

<!-- each page needs to be in a container -->
<main role="main" class="container-fluid">
	<div class="row">
		<br/>
		<div class="col">
			<h1>ADD FAULT</h1>
		</div>
		<div class="col">
			<button type="button" class="btn btn-success float-right" onclick="saveFault()">SAVE</button>
		</div>
	</div>
	<div class="row">
		<div class="col-md">
			<br/>
			<h3> FAULT DETAILS </h3>
			<form>
				<div class="form-group">
					<label for="dateadded"> Date Added </label>
					<input type="text" class="form-control" id="dateadded" readonly />
				</div>
				<textarea class="form-control" id="faultdesc" name="faultdesc" rows="3" placeholder="Fault details"></textarea>						
			</form>		
			<br/>
			<table id="selectedCarTable" class="table"> 
				<thead>
					<tr>
						<th scope="col"></th>
						<th scope="col">Make</th>
						<th scope="col">Model</th>
						<th scope="col">Year</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						
					</tr>
				</tbody>
			</table>
		</div>
		<br/>
		<div class="col-md">
			<br/>
			<div class="row">
				<h3>ADD CAR TO FAULT</h3>
			</div>
			<br/>
			<br/>
			<div class="row">
				<div class="col">Filter:</div>
				<div class="col">
					<select id="selectMake" class="form-control">
						<option value="-1">Select</option>
					</select>
				</div>
				<div class="col">
					<select id="selectYear" class="form-control">
						<option value="-1">Select</option>
					</select>
				</div>
			</div>		
			<br/>
			<div class="row">
				<table id="searchCarTable" class="table">
					<thead>
						<tr>
							<th scope="col"></th>
							<th scope="col">Make</th>
							<th scope="col">Model</th>
							<th scope="col">Year</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="4" class="">Change the filter to show cars</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<!-- Toast to show when data is updated -->
	<div class="toast" role="alert" aria-live="polite" aria-atomic="true" data-delay="1000">
	  <div class="toast-header">
		<strong class="mr-auto">CheckYourCar</strong>
		<small class="text-muted">just now</small>
		<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
	  <div class="toast-body">
		Fault is saved
	  </div>
	</div>

</main>


<?php

//each page requires a footer
require_once("includes/footer.php");
?>
<script type="text/javascript" charset="utf">

var selectedCars = [];
//get the fault id that is loaded or null if empty
var faultid = "<?php echo $faultid ?>";
(function($) {
	loadDetails(faultid);
	loadCarTable(true);
}(jQuery));



//if a fault id is specified load the details into the fields
function loadDetails(faultid) {
	if(faultid > -1) {
		$.getJSON("includes/query.php?function=faultDetails&faultid="+faultid, function(response) {
			var date = new Date(response.result.timeadded);
			var dateStr = new String(date.getDate()).padStart(2, "0")+"/"+(date.getMonth()+1)+"/"+date.getFullYear();
			
			$("#dateadded").val(dateStr);
			$("#faultdesc").val(response.result.faultdesc);
			
			$.each(response.result.cars, function() {
				addToSelectedCar(this.carid);
			});
		});
	}
}



//Removes the item from the selected cars and returns it to the search options
function removeFromSelectedCar(carToRemove) {
	var index = selectedCars.indexOf(carToRemove);
	if(index > -1) {
		selectedCars.splice(index, 1);
	}
	
	$("#selectedCarTable ."+carToRemove).remove();
	
	var make = $("#selectMake option:selected");
	var year = $("#selectYear option:selected").val();
	
	if(make.val() != -1 && year != -1) {
		searchCar(make.text(), year);
	}
}

//save the fault to the database
function saveFault() {
	//create a data object to send with the get request
	var data = {
		"function" : "saveFault",
		"faultid" : faultid,
		faultdesc : $("#faultdesc").val(),
		cars: selectedCars
	};
	
	$.getJSON("includes/query.php", data, function(response) {
		var id = response.result.faultid;
		if(Number.isInteger(id)) {
			faultid = id;
		}
	});
	
	$(".toast").toast({delay: 2000}).toast("show");
}

</script>
