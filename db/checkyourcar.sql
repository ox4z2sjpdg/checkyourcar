-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 03, 2020 at 08:28 AM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `checkyourcar`
--
CREATE DATABASE IF NOT EXISTS `checkyourcar` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `checkyourcar`;

-- --------------------------------------------------------

--
-- Table structure for table `cyc_car_fault`
--

DROP TABLE IF EXISTS `cyc_car_fault`;
CREATE TABLE IF NOT EXISTS `cyc_car_fault` (
  `faultid` int(11) NOT NULL,
  `carid` varchar(125) NOT NULL,
  PRIMARY KEY (`faultid`,`carid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cyc_fault`
--

DROP TABLE IF EXISTS `cyc_fault`;
CREATE TABLE IF NOT EXISTS `cyc_fault` (
  `faultid` int(11) NOT NULL AUTO_INCREMENT,
  `faultdesc` text NOT NULL,
  `timeadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`faultid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cyc_user`
--

DROP TABLE IF EXISTS `cyc_user`;
CREATE TABLE IF NOT EXISTS `cyc_user` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phone` varchar(40) DEFAULT NULL,
  `password` char(32) NOT NULL,
  UNIQUE KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cyc_user_car`
--

DROP TABLE IF EXISTS `cyc_user_car`;
CREATE TABLE IF NOT EXISTS `cyc_user_car` (
  `userid` int(11) NOT NULL,
  `carid` varchar(125) NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cyc_car_fault`
--
ALTER TABLE `cyc_car_fault`
  ADD CONSTRAINT `cyc_car_fault_ibfk_1` FOREIGN KEY (`faultid`) REFERENCES `cyc_fault` (`faultid`);

--
-- Constraints for table `cyc_user_car`
--
ALTER TABLE `cyc_user_car`
  ADD CONSTRAINT `cyc_user_car_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `cyc_user` (`userid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
