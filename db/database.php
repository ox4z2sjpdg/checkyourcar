<?php
//https://devjunky.com/Creating-a-MySqli-Database-Class-in-PHP/

	class Database {
		private $connection = null;
		
		public function __construct( $dbhost = "localhost", $dbname = "checkyourcar", $username = "root", $password = ""){

			try{
		
				$this->connection = new mysqli($dbhost, $username, $password, $dbname);
			
				if( mysqli_connect_errno() ){
					throw new Exception("Could not connect to database.");   
				}
			
			}catch(Exception $e){
				throw new Exception($e->getMessage());   
			}			
		
		}
				
		// Select a row/s in a Database Table
		public function Select( $query = "" , $params = [] ){		
			
			try{
			
				$stmt = $this->executeStatement( $query , $params );
			
				$result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);				
				$stmt->close();
			
				return $result;
			
			}catch(Exception $e){
				throw New Exception( $e->getMessage() );
			}
		
			return false;
		}

		// Update a row/s in a Database Table
		public function Update( $query = "" , $params = [] ){
			$lastid = null;
			try{
			
				//$this->executeStatement( $query , $params )->close();
				
				$stmt = $this->executeStatement( $query , $params );
				$lastid = $stmt->insert_id;
				$stmt->close();
			
			}catch(Exception $e){
				throw New Exception( $e->getMessage() );
			}
		
			return $lastid;
		}		

		// Remove a row/s in a Database Table
		public function Remove( $query = "" , $params = [] ){
			try{
			
				$this->executeStatement( $query , $params )->close();
			
			}catch(Exception $e){
				throw New Exception( $e->getMessage() );
			}
		
			return false;
		}		

		// execute statement
		private function executeStatement( $query = "" , $params = [] ){
		
			try{
			
				$stmt = $this->connection->prepare( $query );
			
				if($stmt === false) {
					throw New Exception("Unable to do prepared statement: " . $query);
				}
			
				if( $params ){
					call_user_func_array(array($stmt, 'bind_param'), $params );				
				}
			
				$stmt->execute();			
				return $stmt;
			
			}catch(Exception $e){
				throw New Exception( $e->getMessage() );
			}
		
		}
		
	}


?>