//AUTHOR - STEPHANIE DELORENZIS

//add the selected car to the selected cars table
function addToSelectedCar(carToAdd) {
	selectedCars.push(carToAdd);
	
	var make = $("#selectMake option:selected");
	var year = $("#selectYear option:selected").val();
	

	$("#searchCarTable ."+carToAdd).remove();	
	
	var carOpt = carToAdd.split("_");
	$("#selectedCarTable tbody").append($("<tr/>").attr("class", carToAdd)
				.append($("<td/>")
					.append($("<button/>").attr("type", "button").attr("class", "btn btn-danger btn-block").attr("onclick", "removeFromSelectedCar(\""+carToAdd+"\")").html("<i class=\"fas fa-minus\"></i>")))
				.append($("<td/>").text(carOpt[0]))
				.append($("<td/>").text(carOpt[1]))
				.append($("<td/>").text(carOpt[2])));
}

function loadCarTable(withButton) {
	//setup the search area
	//reduce the load time for testing
	var makes = ["Suzuki","Toyota","Mazda","BMW","Ford","Holden"];
	for(make in makes) {
		$("#selectMake").append($("<option/>").val(make).text(makes[make]));
	}
	
	var currentYear = new Date().getFullYear();
	var years = [];
	var startYear = 1970;
	while(currentYear > startYear) {
		$("#selectYear").append($("<option/>").val(currentYear).text(currentYear));		
		currentYear--;
	}
	
	
	$("#selectMake").change(function() {
		$("#selectModel").removeAttr("disabled");		
		//update the model
		var make = $(this).find("option:selected");
		var year = $("#selectYear option:selected").val();
		
		if(make.val() != -1 && year != -1) {
			searchCar(make.text(), year, withButton);
		}	
	});
	
	$("#selectYear").change(function() {
		var make = $("#selectMake option:selected");
		var year = $(this).find("option:selected").val();
		
		if(make.val() != -1 && year != -1) {
			searchCar(make.text(), year, withButton);
		}
	});
}


// search for the car using the NHSTA api
function searchCar(make, year, withButton) {
	$("#searchCarTable tbody").empty();
	var query = "https://vpic.nhtsa.dot.gov/api/vehicles/getmodelsformakeyear/make/"+make + "/modelyear/"+year + "/vehicleType/car?format=json";
	
	$.getJSON(query, function(response) {
		//sort the results by the model name
		var arr = response.Results.sort(function(a,b) {
			return a.Model_Name.localeCompare(b.Model_Name);
		});
		
		//filter the array to not include those that are in the selected table
		var arr = $.grep(arr, function(v) {
			return $.inArray(v.Make_Name+"_"+v.Model_Name+"_"+year, selectedCars) === -1;
		});
		
		//add a row for each result
		$.each(arr, function() {
			var carString = this.Make_Name+"_"+this.Model_Name+"_"+year;
			var table = $("#searchCarTable tbody");
			
			table.append($("<tr/>").attr("class", carString).append($("<td/>").text(this.Make_Name))
				.append($("<td/>").text(this.Model_Name))
				.append($("<td/>").text(year)));
			
			//if we need to add the plus button - do it now
			if(withButton) {
				table.find("tr."+carString).prepend($("<td/>")
					.append($("<button/>").attr("type", "button").attr("class", "btn btn-success btn-block").attr("onclick", "addToSelectedCar(\""+carString+"\")").html("<i class=\"fas fa-plus\"></i>")));
			}
		});
	});
}