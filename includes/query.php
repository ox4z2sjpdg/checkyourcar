<?php
//AUTHOR - STEPHANIE DELORENZIS

	include_once("../db/database.php");

	//to trigger a function query the function name
	//eg: query.php?function=getAllMakes

	if(isset($_GET["function"])) {
		switch($_GET["function"]) {
			case "getAllMakes":
				getAllMakes();
				break;
			case "getFaults":
				getAllFaults();
				break;
			case "removeFault":
				removeFault();
				break;
			case "faultDetails":
				getFaultDetails();
				break;
			case "saveFault":
				saveFault();
				break;
			default:
				//function not found
		}
	}
	
	//get all the faults saved in the database
	function getAllFaults() {
		$db = new Database();
		$data = $db->Select("SELECT * FROM cyc_fault");
		
		header('Content-Type: application/json');
		echo json_encode(array(
			"code" => 200,
			"result" => $data
		));
	}
	
	//remove a specified fault
	function removeFault() {
		if(isset($_GET["faultid"]) && is_numeric($_GET["faultid"])) {
			$faultid = intval($_GET["faultid"]);
			
			$db = new Database();
			
			//delete cars first
			$db->Remove("DELETE FROM cyc_car_fault WHERE faultid = ?", ["i", &$faultid]);
			//then delete the fault		
			$db->Remove("DELETE FROM cyc_fault WHERE faultid = ?", ["i", &$faultid]);
		
			echo json_encode(array(
				"code" => 200
			));		
		
			return;
		}
		
		echo json_encode(array(
			"code" => 400,
			"result" => null
		));		
	}
	
	//save the fault
	function saveFault() {
		header('Content-Type: application/json');
		$faultid = null;
		if(isset($_GET["faultid"]) && is_numeric($_GET["faultid"])) {
			echo "FAULT ID SET";
			$faultid = intval($_GET["faultid"]);
		}
		
		$faultdesc = $_GET["faultdesc"];
		//if no cars returns error
		$cars = isset($_GET["cars"]) ? $_GET["cars"] : [];
		
		$db = new Database();
		
		if($faultid == null) {
			//insert into fault
			$faultid = $db->Update("INSERT INTO cyc_fault(faultdesc) VALUES (?)", ["s",&$faultdesc]);		
		} else {
			//update fault
			$db->Update("UPDATE cyc_fault SET faultdesc = ? WHERE faultid = ?", ["si", &$faultdesc, &$faultid]);
		}	
		
		//remove all cars for this fault
		//add all cars for this fault
		//insert into reference table
		$db->Remove("DELETE FROM cyc_car_fault WHERE faultid = ?", ["i", &$faultid]);
		foreach($cars as $car) {
			$db->Update("INSERT INTO cyc_car_fault(faultid, carid) VALUES(?,?)", ["is", &$faultid, &$car]);
		}
		
		echo json_encode(array(
			"code" => 200,
			"result" => array("faultid" => $faultid)
		));			
	}
	
	//get the fault details including all the cars
	function getFaultDetails() {
		header('Content-Type: application/json');
		if(isset($_GET["faultid"]) && is_numeric($_GET["faultid"])) {
			$faultid = intval($_GET["faultid"]);
						
			$db = new Database();
			$result = $db->Select("SELECT * FROM cyc_fault WHERE faultid = ?", ["i", &$faultid]);
			
			if(count($result) == 1) {
				$data = array("faultid"=>$faultid, 
					"faultdesc"=>$result[0]["faultdesc"], 
					"timeadded"=>$result[0]["timeadded"],
					"cars" => []);
			
			
			
				$carResult = $db->Select("SELECT carid FROM cyc_car_fault WHERE faultid = ?", ["i", &$faultid]);
				foreach($carResult as $car) {
					array_push($data["cars"], $car);
				}
				
				echo json_encode(array(
					"code" => 200,
					"result" => $data
				));
				
				return;
			}
		} 
		
		echo json_encode(array(
			"code" => 400,
			"result" => []
		));		
	}
?>