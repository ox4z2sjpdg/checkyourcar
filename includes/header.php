<?php
//AUTHOR - STEPHANIE DELORENZIS
?>
<!doctype html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
		
		<!-- custom css -->
		<link rel="stylesheet" href="css/main.css">
		
		<title>Check Your Car</title>
	</head>
	<body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="#">CheckYourCar</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
              <a class="nav-link active" href="index.php">Home <span class="sr-only">(current)</span></a>
              <!-- only show these if logged in -->
              <a class="nav-link" href="#">User Details</a>
              <a class="nav-link" href="#">Car Details</a>
              <!-- only show this if logged in as admin -->
			  <a class="nav-link" href="viewfault.php">Faults</a>

              <!-- Button to sign out -->
              <!-- <a class="nav-link" href="signout.php">Sign Out</a> -->
              <!-- sign in button -->
            </div>
          </div>
        </nav>
