<?php
//AUTHOR - STEPHANIE DELORENZIS

//each page requires a header
require_once("includes/header.php");

?>
<!-- each page needs to be in a container -->
<main role="main" class="container">
  <h1>Search Car</h1>
  
  <div class="row">
		<div class="col">Filter:</div>
		<div class="col">
			<select id="selectMake" class="form-control">
				<option value="-1">Select</option>
			</select>
		</div>
		<div class="col">
			<select id="selectYear" class="form-control">
				<option value="-1">Select</option>
			</select>
		</div>
	</div>		
	<br/>
	<div class="row">
		<table id="searchCarTable" class="table">
			<thead>
				<tr>
					<th scope="col">Make</th>
					<th scope="col">Model</th>
					<th scope="col">Year</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="4" class="">Change the filter to show cars</td>
				</tr>
			</tbody>
		</table>
	</div>
</main>

<?php
//each page requires a footer
require_once("includes/footer.php");
?>
<script type="text/javascript" charset="utf">
var selectedCars = [];
(function($) {
	loadCarTable(false);
}(jQuery));
</script>
